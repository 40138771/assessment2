﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
   public class Students:BasicDetails
    {
        // Declared all variables or properties
        private int matriculationNum;
        private int id;

        // Declare a ID property of type int
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        //This method contains the get and set methods and validation checks for property matriculationNum
        public int MatriculationNum
        {
            get
            {
                return matriculationNum;
            }
            set
            {
              matriculationNum = value;
            }
        }
    }
}
