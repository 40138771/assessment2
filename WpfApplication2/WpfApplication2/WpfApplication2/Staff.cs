﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
      public class Staff:BasicDetails
    {
        // Declared all variables or properties
        private string department;
        private string role;
        private int payrollNum;
        public List<int> payrollNumList = new List<int>();
      
        //This method contains the get and set methods and validation checks for property department
        public string Department
        {
            get
            {
                return department;
            }
            set
            {
                   department = value;
            }
        }

        //This method contains the get and set methods and validation checks for property role
        public string Role
        {
            get
            {
                return role;
            }
            set
            {
                role = value;
            }
        }

        //This method contains the get and set methods and validation checks for property payrollNum
        public int PayrollNum
        {
            get
            {
                return payrollNum;
            }
            set
            {
                    payrollNum = value; 
            }
        }
    }
}
