﻿/* 
*
* Written by Okorie Chinaza Medeline 12/12/2014
* The class Gui contains code that enable the functions of the tools to work as specified in the first window called Gui
* 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for AddModule.xaml
    /// </summary>
    public partial class AddModule : Window
    {
        public MainWindow Parent { get; set; }
        public Students StudentDisplay { get; set; }


            AddStaff addStaff = new AddStaff();
            Staff staff1 = new Staff();
            Modules module = new Modules();
            List<string > moduleList = new List<string>();

        public AddModule()
        {
           InitializeComponent();
        }
         private void btnModuleAdd_Click_1(object sender, RoutedEventArgs e)
        {
            var moduleCodeList = new List<string>();
            if (tbModuleName.Text == "" | tbModuleCode.Text == "")// | tbModuleLeader.Text == "" )// Validation to check that no textbox is left blank
                {
                    MessageBox.Show("Enter all fields !");  // Error message if the statement is false
                }
            else if (moduleCodeList.Contains(tbModuleCode.Text)) //Checks if value is a unique value from values in the list
            {
                MessageBox.Show("wrong input. Please retype"); //Error if the statement is false
            }
            else
            {
                moduleCodeList.Add(tbModuleCode.Text); // adds value to list
                bool isUnique = moduleCodeList.Distinct().Count() == moduleCodeList.Count(); //checks if all values in list are unique

                string name = tbModuleName.Text;
                module.Name = name;


                string moduleCode = tbModuleCode.Text;
                module.ModuleCode = moduleCode;

              
               //module.ModuleLeader.Name = moduleLeaderDropList.


                moduleList.Add(module.Name);
                moduleList.Add(module.ModuleCode);
              //  moduleList.Add(module.ModuleLeader);

                MessageBox.Show("module saved");

                tbModuleName.Clear();

                tbModuleCode.Clear();

                //tbModuleLeader.Clear();

            }
        }

        private void btnShowModules_Click(object sender, RoutedEventArgs e)
        {
            moduleRecordList.Items.Add(module.Name + "," + " " + module.ModuleCode+ " ");
        }

        private void btnModuleAdd_Click(object sender, RoutedEventArgs e)
        {
             moduleLeaderDropList.Items.Add(staff1.Name + "," + " " + staff1.Address + "," + " " + staff1.Email + "," + " " + staff1.PayrollNum + "," + " " + staff1.Department + "," + " " + staff1.Role + " ");
        }
        
        }
    }

