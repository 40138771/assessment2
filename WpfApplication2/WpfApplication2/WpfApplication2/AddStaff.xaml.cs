﻿/* 
*
* Written by Okorie Chinaza Medeline 12/12/2014
* The class Gui contains code that enable the functions of the tools to work as specified in the first window called Gui
* 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for AddStaff.xaml
    /// </summary>
    public partial class AddStaff : Window
    {
        public MainWindow Parent { get; set; }
        public Students studentDisplay { get; set; }
        Staff staff1 = new Staff();
       public List<string > staffList = new List<string>();
       
        public AddStaff()
        {
            InitializeComponent();
       }

        
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var roleList = new List<string>();
            roleList.Add("Lecturer");
            roleList.Add("Professor");
            roleList.Add("Senior Lecturer");

            // This enables the add button on the gui form to update Staff class with values in textboxes when clicked

            if (tbFullName.Text == "" | tbAddress.Text == "" | tbEmail.Text == "" | tbDepartment.Text == ""
              | (tbRole.Text == "" | tbPayRoleNum.Text == ""))// Validation to check that no textbox is left blank
            {
                MessageBox.Show("Enter all fields !");  // Error message if the statement is false
            }
            else if ((tbEmail.Text).Contains("@") == false)
            {
                MessageBox.Show("wrong input ... missing '@'");
            }
            else if (roleList.Contains((tbRole.Text) , StringComparer.OrdinalIgnoreCase) == false )
            {
                MessageBox.Show("wrong input.. please type required role");
            }
            else if ((int.Parse(tbPayRoleNum.Text)) < 9000 || (int.Parse(tbPayRoleNum.Text)) > 9999) // The if statement is a validation that value is in range 9000 to 9999
            {
                MessageBox.Show("Error : Payroll Number out of range"); //Error if the statement is false
            }
            else if (staff1.payrollNumList.Contains(int.Parse(tbPayRoleNum.Text))) //Checks if value is a unique value from values in the list
            {
                MessageBox.Show("Error :PayRoll Number not unique"); //Error if the statement is false
            }
            else
            {
                staff1.payrollNumList.Add(int.Parse(tbPayRoleNum.Text)); //Adds value to list
                //bool isUnique = payrollNumList.Distinct().Count() == payrollNumList.Count(); //checks if all values in the list is unique
                //saves the text in the textbox

                string Name = tbFullName.Text;
                staff1.Name = Name;


                string address = tbAddress.Text;
                staff1.Address = address;


                string email= tbEmail.Text;
                staff1.Email = email;


                string department = tbDepartment.Text;
                staff1.Department = department;


                string role = tbRole.Text;
                staff1.Role = role;


                int payrollNum = int.Parse(tbPayRoleNum.Text); // converts string input to int
                staff1.PayrollNum = payrollNum;


                staffList.Add(staff1.Name);
                staffList.Add(staff1.Email);
                staffList.Add(staff1.Department);
                staffList.Add(staff1.Address);
                staffList.Add((staff1.PayrollNum).ToString());
                staffList.Add(staff1.Role);

                MessageBox.Show("staff saved");

                tbFullName.Clear();

                tbAddress.Clear();

                tbEmail.Clear();

                tbDepartment.Clear();

                tbRole.Clear();

                tbPayRoleNum.Clear();

            }
        }

        private void ShowAllStaff_Click(object sender, RoutedEventArgs e)
        {

            recordList.Items.Add(staff1.Name + "," +" "+ staff1.Address + "," +" "+ staff1.Email + ","+" " + staff1.PayrollNum +"," + " " + staff1.Department + "," +" "+staff1.Role+" ");
            foreach (var v in staffList){
            System.IO.StreamWriter file = new System.IO.StreamWriter("NapierAdmin");
            file.WriteLine(v);
            file.Close();
            }
        }
 
    }

}

