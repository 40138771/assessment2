﻿/* 
*
* Written by Okorie Chinaza Medeline 12/12/2014
* The class Gui contains code that enable the functions of the tools to work as specified in the first window called Gui
* 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
    public class BasicDetails
    {
        // Declared all variables or properties
        private string name;
        private string address;
        private string email;

        //This method contains the get and set methods and validation checks for property name
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }


        //This method contains the get and set methods and validation checks for property address
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        //This method contains the get and set methods and validation checks for property email
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }


    }
}
