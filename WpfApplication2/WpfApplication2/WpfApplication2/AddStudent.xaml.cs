﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for AddStudent.xaml
    /// </summary>
    public partial class AddStudent : Window
    {
        public MainWindow Parent { get; set; }
        Students student = new Students();
        public List<string> studentsList = new List<string>();
        
        public AddStudent()
        {
            InitializeComponent();
        }

        private void btnStudentSave_Click(object sender, RoutedEventArgs e)
        {
             var matricNumList = new List<string>();
            // This enables the add button on the gui form to update Staff class with values in textboxes when clicked

            if (tbStudentFullName.Text == "" | tbStudentAddress.Text == "" | tbStudentEmail.Text == "" | tbMatricNo.Text == "")// Validation to check that no textbox is left blank
                {
                   MessageBox.Show("Enter all fields !");  // Error message if the statement is false
               }
            else if ((tbStudentEmail.Text).Contains("@") == false)
               {
                    //MessageBox.Show("wrong input ... missing '@'");
               }
            else if (int.Parse(tbMatricNo.Text) < 1000 || int.Parse(tbMatricNo.Text) > 9000) // The if statement is a validation that value is in range 1000 to 9000
                {
                     MessageBox.Show("Error : Matriculation Number out of range"); //Error if the statement is false
                }
           else if (matricNumList.Contains(tbMatricNo.Text)) //Checks if value is a unique value from values in the list
                {
                     MessageBox.Show("Error : Number out of range"); //Error if the statement is false
                }
           else
                {
                    matricNumList.Add(tbMatricNo.Text); //adds value to list
                    bool isUnique = matricNumList.Distinct().Count() == matricNumList.Count(); //checks if all values in list are unique

                string name = tbStudentFullName.Text;
                student.Name = name;


                string address = tbStudentAddress.Text;
                student.Address = address;


                string email = tbStudentEmail.Text;
                student.Email = email;

                int matriculationNum = int.Parse(tbMatricNo.Text); // converts string input to int
                student.MatriculationNum = matriculationNum;

                studentsList.Add(student.Name);
                studentsList.Add(student.Email);
                studentsList.Add(student.Address);
                studentsList.Add((student.MatriculationNum).ToString());

                MessageBox.Show("student saved");

                tbStudentFullName.Clear();

                tbStudentAddress.Clear();

                tbStudentEmail.Clear();

                tbMatricNo.Clear();
                }
        }

        private void btnShowStudent_Click(object sender, RoutedEventArgs e)
        {
            studentRecordList.Items.Add(student.Name + "," + " " + student.Address+ "," + " " + student.Email + "," + " " + student.MatriculationNum + " ");
        }

       
    }
}
