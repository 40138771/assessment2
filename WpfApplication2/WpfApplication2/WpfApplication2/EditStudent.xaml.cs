﻿/* 
*
* Written by Okorie Chinaza Medeline 12/12/2014
* The class Edit is a wpf window that student is a 
* 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for EditStudent.xaml
    /// </summary>
    public partial class EditStudent : Window
    {
        public EditStudent()
        {
            InitializeComponent();
        }
        public MainWindow Parent { get; set; }

        private void btnStudentSave_Click(object sender, RoutedEventArgs e)
        {
            Parent.studentEdit.Name = tbStudentFullName.Text;
            Parent.studentEdit.Address = tbStudentAddress.Text;
            Parent.studentEdit.Email = tbStudentEmail.Text;
            Parent.studentEdit.MatriculationNum = int.Parse(tbMatricNo.Text);

            this.Visibility = Visibility.Hidden;
        }
    }
}
