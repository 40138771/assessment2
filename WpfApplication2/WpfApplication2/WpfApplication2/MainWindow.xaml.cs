﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;


namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow: Window
    {
        public Students studentEdit = null;

        //creating an object of the student window
        private AddStudent addStudent = new AddStudent();
        private AddModule addModule = new AddModule();
        private AddStaff addStaff = new AddStaff();
        private EditStudent editStudent = new EditStudent();

        //create a list of students
        private List<Students> StudentsDisplay = new List<Students>();

        public MainWindow()
        {
            InitializeComponent();
            addStudent.Parent = this;
            addStaff.Parent = this;
            addModule.Parent = this;
            editStudent.Parent = this;
            Refresh();
        }

      
        private void btnAddStudent_Click(object sender, RoutedEventArgs e)
        {
           AddStudent ShowAddStudent = new AddStudent();
            ShowAddStudent.Show();

        }

        public void Refresh()
        {
            studentsList.Items.Clear();

            foreach (var v in StudentsDisplay )
            {
                studentsList.Items.Add(v.Name + "," + " " + v.Address + "," + " " + v.Email + "," + " " + v.MatriculationNum + " ");
                StudentsDisplay.Clear();
                StudentsDisplay.Add(v);
            }
        }

        private void btnAddStaff_Click(object sender, RoutedEventArgs e)
        {
            AddStaff ShowAddStaff = new AddStaff();
            ShowAddStaff.Show();

        }

        private void btnAddModule_Click(object sender, RoutedEventArgs e)
        {
            AddModule ShowAddModule = new AddModule();
            ShowAddModule.Show();
        }

       private void btnEditStudent_Click(object sender, RoutedEventArgs e)
        {
            // open and shows the edit window button
            addStaff.studentDisplay = SelectedStudent();
            if (string.IsNullOrEmpty(addStaff.studentDisplay.Name))
            {
                //Display a  message if no student is selected
                System.Windows.MessageBox.Show("Please select a student first");
                return;
            }

            {
                studentEdit = SelectedStudent();
                editStudent.tbStudentFullName.Text = studentEdit.Name;
                editStudent.tbStudentAddress.Text = studentEdit.Address;
                editStudent.tbStudentEmail.Text = studentEdit.Email;
                editStudent.tbMatricNo.Text = studentEdit.MatriculationNum.ToString();

                editStudent.Visibility = Visibility.Visible;
            }
        }

        private Students SelectedStudent()
        {
            try
            {
                int studentId = StudentsDisplay.ElementAt(studentsList.SelectedIndex).Id;
            }
            catch
            {
                MessageBox.Show("Student not found");
            }
            return new Students();
        }

        //method removes a student
        public void DeleteStudent()
        {
            try
            {
                //Find selected journey using ID
                int studentId = SelectedStudent().Id;
            }
            catch
            {
            }
        }

        private void btnRemoveStudent_Click(object sender, RoutedEventArgs e)
        {
            DeleteStudent();
            Refresh();
        }

        private void btnViewAllStudents_Click(object sender, RoutedEventArgs e)
        {
            studentsList.Items.Clear();
            foreach (var v in StudentsDisplay )
            {
                studentsList.Items.Add(v.Name + "," + " " + v.Address + "," + " " + v.Email + "," + " " + v.MatriculationNum + " ");
            }
        }

    }
}
