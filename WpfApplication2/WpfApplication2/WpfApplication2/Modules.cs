﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication2
{
    public class Modules:BasicDetails
    {
        // Declared all variables or properties
        private string moduleCode;
        private Staff moduleLeader1;
       

        //This method contains the get and set methods and validation checks for property moduleCode
        public string ModuleCode
        {
            get
            {
                return moduleCode;
            }
            set
            {
               moduleCode = value;
            }
        }
            

       public Staff ModuleLeader
        {
            get
            {
                return moduleLeader1;
            }
            set
            {
                moduleLeader1 = value;
            }
        }
    }
}
